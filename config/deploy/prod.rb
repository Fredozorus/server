role :app, %w{deploy@xwinga.com}
set :branch, 'master'
set :deploy_to, '/home/deploy/android.mrzee.fr/prod/server'
set :keep_releases, 1

namespace :deploy do
  after  :updating, 'sublet:docker'
  after  :updating, 'sublet:vhost'
  after  :publishing, 'sublet:reload_nginx'
  before :cleanup, 'sublet:remove_releases'
end