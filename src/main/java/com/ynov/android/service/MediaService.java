package com.ynov.android.service;

import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.ynov.android.domain.Media;
import com.ynov.android.exception.UserNotFoundException;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by tom on 12/01/2017.
 */
@Service
public class MediaService {

    @Autowired
    private GridFS mediaGridFs;

    public Map<String,Object> upload(Media media) throws IOException {
        GridFSInputFile file = mediaGridFs.createFile(media.getFile().getInputStream());
        file.setFilename(media.getFile().getOriginalFilename());
        file.setContentType(media.getFile().getContentType());
        if(media.getMetaData() != null && media instanceof Map){
            file.setMetaData(new BasicDBObject(media.getMetaData()));
        }
        file.save();
        return toMap(file);
    }

    public Iterable<Map> all() {
        return newArrayList(transform(mediaGridFs.find(new BasicDBObject()), MediaService::toMap));
    }

    public GridFSDBFile serve(String id) throws Exception {
        GridFSDBFile file = mediaGridFs.find(new ObjectId(id));
        if(file != null){
            return file;
        }
        throw new UserNotFoundException(String.format("Could not find file with id %s", id));
    }

    public static Map toMap(GridFSFile file){
        return new ImmutableMap.Builder()
                .put("metaData", file.getMetaData() != null ? file.getMetaData() : new HashMap<>())
                .put("id", file.getId().toString())
                .put("filename", file.getFilename())
                .build();
    }
}
