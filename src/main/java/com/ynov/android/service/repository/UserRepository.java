package com.ynov.android.service.repository;

import com.ynov.android.domain.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by tom on 12/01/2017.
 */
@Service
public class UserRepository extends AbstractRepository<User> {
    public Optional<User> findOneByUsername(String username) {
        return Optional.ofNullable(collection.findOne("{ username: # }", username).as(User.class));
    }
}
