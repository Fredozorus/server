package com.ynov.android.service.repository;

import com.ynov.android.annotation.Document;
import com.ynov.android.domain.core.EntityAble;
import com.ynov.android.domain.core.TimestampableEntity;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.util.Optional;

/**
 * Created by tom on 25/07/2016.
 */
public class AbstractRepository<T extends EntityAble> implements InitializingBean {

    protected Class<T> clazz;
    protected MongoCollection collection;
    @Autowired
    protected Jongo jongo;

    @Override
    public void afterPropertiesSet() throws Exception {

        clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

        for(Annotation annotation : clazz.getAnnotations()){
            if(annotation instanceof Document){
                collection = jongo.getCollection(((Document) annotation).name());
            }
        }

        if(collection == null){
            collection = jongo.getCollection(clazz.getSimpleName().toLowerCase());
        }
    }

    public Iterable<T> all() {
        return collection.find().as(clazz);
    }

    public Iterable<T> query(String query, Object... args ){
        return collection.find(query,args).as(clazz);
    }

    public void delete(String id){
        collection.remove(new ObjectId(id));
    }

    public Optional<T> byId(String id){
        return Optional.ofNullable(collection.findOne(new ObjectId(id)).as(clazz));
    }

    public T save(T instance){

        if(instance instanceof TimestampableEntity){
            if(instance.getId() != null){
                ((TimestampableEntity) instance).setUpdated(DateTime.now());
            }else{
                ((TimestampableEntity) instance).setCreated(DateTime.now());
                ((TimestampableEntity) instance).setUpdated(DateTime.now());
            }
        }

        if(instance.getId() != null){
            collection.update(new ObjectId(instance.getId())).with(instance);
        }else{
            collection.save(instance);
        }

        return instance;
    }

    public <Z> Z oneAs(String id, Class<Z> clazz){
        return collection.findOne(new ObjectId(id)).as(clazz);
    }

    public <Z> Z oneAs(Class<Z> clazz, String query, Object ... args){
        return collection.findOne(query, args).as(clazz);
    }
}
