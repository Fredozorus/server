package com.ynov.android.service;

import com.ynov.android.domain.LoginToken;
import com.ynov.android.domain.User;
import com.ynov.android.exception.BadCredentialsException;
import com.ynov.android.exception.UserNotFoundException;
import com.ynov.android.security.Role;
import com.ynov.android.service.repository.LoginTokenRepository;
import com.ynov.android.service.repository.UserRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;
import static com.ynov.android.utils.MorePreconditions.checkPresent;

/**
 * Created by tom on 12/01/2017.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LoginTokenRepository loginTokenRepository;

    public LoginToken findUserAndLogin(User user) {
        User reference = checkPresent(userRepository.findOneByUsername(user.getUsername()), new UserNotFoundException());
        if(!BCrypt.checkpw(user.getPassword(), reference.getPassword())){
            throw new BadCredentialsException();
        }
        return loginTokenRepository.save(new LoginToken(reference.getId()));
    }

    public void logout(String token){
        loginTokenRepository.revokeForToken(token);
    }

    public User create(User user) {
        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        user.setRoles(newArrayList(Role.USER));
        return userRepository.save(user);
    }

    public User update(String userId, User user) {
        User reference = checkPresent(userRepository.byId(userId), new UserNotFoundException());
        reference.setEmail(user.getEmail());
        reference.setFirstname(user.getFirstname());
        reference.setLastname(user.getLastname());
        reference.setMetaData(user.getMetaData());
        return userRepository.save(reference);
    }

    public Iterable<User> all() {
        return userRepository.all();
    }

    public User userWithId(String userId) {
        return checkPresent(userRepository.byId(userId), new UserNotFoundException());
    }

    public User getUserForToken(String authorization) {
        LoginToken token = checkPresent(loginTokenRepository.findToken(authorization), new UserNotFoundException());
        return checkPresent(userRepository.byId(token.getUserRef()), new UserNotFoundException());
    }
}
