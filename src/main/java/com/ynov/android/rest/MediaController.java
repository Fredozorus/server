package com.ynov.android.rest;

import com.mongodb.gridfs.GridFSDBFile;
import com.ynov.android.annotation.Secured;
import com.ynov.android.domain.Media;
import com.ynov.android.security.Role;
import com.ynov.android.service.MediaService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by tom on 12/01/2017.
 */
@RestController
@RequestMapping("/media")
@Secured(roles = Role.USER)
public class MediaController {

    @Autowired
    private MediaService mediaService;

    @RequestMapping("/list")
    public Iterable<Map> testForUser(){
        return mediaService.all();
    }

    @RequestMapping("/upload")
    public Map upload(Media media) throws IOException {
        return mediaService.upload(media);
    }

    @RequestMapping("/{id}")
    public void get(@PathVariable String id, HttpServletResponse response) throws Exception {
        GridFSDBFile file = mediaService.serve(id);
        IOUtils.copy(file.getInputStream(), response.getOutputStream());
        response.setContentType(file.getContentType());
        response.flushBuffer();
    }
}
