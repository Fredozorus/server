package com.ynov.android.config;

import com.ynov.android.security.AuthorizationHeaderInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by tom on 12/01/2017.
 */
@Configuration
public class AppConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private HandlerInterceptor securityInterceptor;
    @Autowired
    private AuthorizationHeaderInterceptor authorizationHeaderInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authorizationHeaderInterceptor);
        registry.addInterceptor(securityInterceptor);
    }
}
