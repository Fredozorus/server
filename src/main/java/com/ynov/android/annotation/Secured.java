package com.ynov.android.annotation;

import com.ynov.android.security.Role;
import com.ynov.android.security.RoleStrategy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by tom on 19/10/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.TYPE})
public @interface Secured {
    Role[] roles() default Role.ALL;
    RoleStrategy strategy() default RoleStrategy.AT_LEAST_ONE;
}
