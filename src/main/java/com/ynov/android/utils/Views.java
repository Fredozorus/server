package com.ynov.android.utils;

/**
 * Created by tom on 25/07/2016.
 */
public class Views {
    public interface Public {}
    public interface Private extends Public {}
    public interface Transient {}
}
