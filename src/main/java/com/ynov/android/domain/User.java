package com.ynov.android.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.ynov.android.domain.core.TimestampableEntity;
import com.ynov.android.security.Role;
import com.ynov.android.utils.Views;
import com.ynov.android.validation.Unique;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tom on 12/01/2017.
 */
public class User extends TimestampableEntity {
    @JsonView(Views.Public.class)
    @NotBlank
    @Unique
    private String username;
    @JsonView(Views.Private.class)
    @NotBlank
    private String password;
    @JsonView(Views.Public.class)
    private String firstname;
    @JsonView(Views.Public.class)
    private String lastname;
    @JsonView(Views.Public.class)
    @NotBlank
    @Email
    private String email;
    @JsonView(Views.Public.class)
    private Map<String, Object> metaData;
    @JsonView(Views.Private.class)
    private List<Role> roles = new ArrayList();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String,Object> metaData) {
        this.metaData = metaData;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
