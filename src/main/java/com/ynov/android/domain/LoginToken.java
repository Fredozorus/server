package com.ynov.android.domain;

import com.ynov.android.domain.core.TimestampableEntity;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.UUID;

/**
 * Created by tom on 12/01/2017.
 */
public class LoginToken extends TimestampableEntity {
    private String token = UUID.randomUUID().toString();
    @MongoObjectId
    private String userRef;
    private boolean active;

    public LoginToken() {
    }

    public LoginToken(String userRef){
        active = true;
        this.userRef = userRef;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
