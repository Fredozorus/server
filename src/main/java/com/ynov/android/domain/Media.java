package com.ynov.android.domain;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by tom on 12/01/2017.
 */
public class Media {

    private MultipartFile file;
    private Map<String, Object> metaData;

    public Media() {
    }

    public Media(MultipartFile file, Map<String, Object> metaData) {
        this.file = file;
        this.metaData = metaData;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String, Object> metaData) {
        this.metaData = metaData;
    }
}
