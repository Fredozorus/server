package com.ynov.android.domain.core;

import com.fasterxml.jackson.annotation.JsonView;
import com.ynov.android.utils.Views;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

/**
 * Created by tom on 25/07/2016.
 */
public class Entity implements EntityAble {

    @MongoObjectId
    @MongoId
    @JsonView(Views.Public.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
