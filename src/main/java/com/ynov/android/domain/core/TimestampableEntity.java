package com.ynov.android.domain.core;

import com.fasterxml.jackson.annotation.JsonView;
import com.ynov.android.utils.Views;
import org.joda.time.DateTime;

/**
 * Created by tom on 25/07/2016.
 */
public class TimestampableEntity extends Entity {

    @JsonView(Views.Public.class)
    private DateTime created = DateTime.now();
    @JsonView(Views.Public.class)
    private DateTime updated = DateTime.now();

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getUpdated() {
        return updated;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }
}