package com.ynov.android.domain.core;

/**
 * Created by tom on 25/07/2016.
 */
public interface EntityAble {
    String getId();
    void setId(String id);
}
