package com.ynov.android.security;

import java.util.Collections;
import java.util.List;

/**
 * Created by tom on 19/10/2016.
 */
public enum RoleStrategy {
    AT_LEAST_ONE {
        @Override
        public void check(List<Role> userRoles, List<Role> roleList) {
            if(userRoles.contains(Role.ALL)){
                return;
            }
            if(Collections.disjoint(userRoles, roleList)){
                throw new SecuredResourceException();
            }
        }
    }, ALL{
        @Override
        public void check(final List<Role> userRoles, List<Role> roleList) {
            if(userRoles.contains(Role.ALL)){
                return;
            }
            roleList.stream().forEach(r -> {
                if(!userRoles.contains(r)){
                    throw new SecuredResourceException();
                }
            });
        }
    };

    public abstract void check(List<Role> userRoles, List<Role> roleList);
}
