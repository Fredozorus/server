package com.ynov.android.security;

/**
 * Created by tom on 19/10/2016.
 */
public enum Role {
    ADMIN, USER, PROMOTER, ALL;
}
