package com.ynov.android.security;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by tom on 19/10/2016.
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class SecuredResourceException extends RuntimeException {
    public SecuredResourceException() {
    }

    public SecuredResourceException(String message) {
        super(message);
    }

    public SecuredResourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecuredResourceException(Throwable cause) {
        super(cause);
    }

    public SecuredResourceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
