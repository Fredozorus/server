package com.ynov.android.validation;

import com.ynov.android.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by tom on 29/08/2016.
 */
public class UniqueValidator implements ConstraintValidator<Unique, String> {

   @Autowired
   private UserRepository userRepository;

   public void initialize(Unique constraint) {
   }

   public boolean isValid(String address, ConstraintValidatorContext context) {
      return address != null && !userRepository.findOneByUsername(address).isPresent();
   }
}
