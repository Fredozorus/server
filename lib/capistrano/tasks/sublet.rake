# encoding: UTF-8

namespace :sublet do

    desc "Creates the docker container"
    task :docker do
        app = fetch(:application)
        stage = fetch(:stage)
        deploy_to = fetch(:deploy_to)
        on roles(:all) do
            execute "docker run -d -P -e PROFILES=#{stage} --name=#{app}-#{stage}-#{release_timestamp} -v #{releases_path}/#{release_timestamp}:/var/www -v /var/log/#{stage}:/var/log/#{stage} xwinga/server"
            sleep 60
        end
    end

    desc "Remove useless containers"
    task :remove_releases do
        on roles(:all) do

            releases = capture(:ls, "-xtr", releases_path).split
            app = fetch(:application)
            stage = fetch(:stage)
            if releases.count >= fetch(:keep_releases)
                directories = (releases - releases.last(fetch(:keep_releases)))
                for release in directories do
                    execute :rm, "/etc/nginx/sites-available/#{app}-#{stage}-#{release} || true"
                    execute "docker rm -f #{app}-#{stage}-#{release} || true"
                end
            end
        end
    end

    desc "Creates the vhost file"
    task :vhost do
        deploy_to = fetch(:deploy_to)
        app = fetch(:application)
        stage = fetch(:stage)
        on roles(:all) do
            port = capture("docker inspect --format='{{(index (index .NetworkSettings.Ports \"8080/tcp\") 0).HostPort}}' #{app}-#{stage}-#{release_timestamp}")
            execute :cp, "#{releases_path}/#{release_timestamp}/vhost-#{stage} /etc/nginx/sites-available/#{app}-#{stage}-#{release_timestamp}"
            execute "sed -i s/@PORT@/#{port}/ /etc/nginx/sites-available/#{app}-#{stage}-#{release_timestamp}"
        end
    end

    desc "Reloads nginx"
    task :reload_nginx do
        app = fetch(:application)
        stage = fetch(:stage)
        on roles(:all) do
            execute :rm, "/etc/nginx/sites-enabled/#{app}-#{stage}-current || true"
            execute :ln, "-s /etc/nginx/sites-available/#{app}-#{stage}-#{release_timestamp} /etc/nginx/sites-enabled/#{app}-#{stage}-current || true"
            execute "sudo service nginx restart || true"
        end
    end
end